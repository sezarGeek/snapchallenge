package cmd

import (
	"github.com/namsral/flag"
	"github.com/sirupsen/logrus"
	"math/rand"
	dbModel "snapChallenge/pkg/db/model"
	"snapChallenge/pkg/db/postgresql"
	"strconv"
	"sync"
	"time"
)

var (
	dbConnectionString  = flag.String("db-connection-string", "host=localhost user=postgres password=3241550877 dbname=snap port=5432 sslmode=disable TimeZone=Asia/Tehran", "ConnectionString")
	verbosity           = flag.Int("verbosity", int(logrus.InfoLevel), "set verbosity level of the log")
	dbMaxIdleConnection = flag.Int("db-max-idle-connection", 100, "DbMaxIdleConnection")
	dbMaxOpenConnection = flag.Int("db-max-open-connection", 100, "DbMaxOpenConnection")
	numberOfVendors     = flag.Int("number-of-vendors", 1000, "NumberOfVendors")
	maximumDelayTime    = flag.Int("maximum-delay-time", 60, "NumberOfVendors")
)

func Run() {
	flag.Parse()
	random := rand.New(rand.NewSource(time.Now().Unix()))
	logger := logrus.New()
	ps, err := postgresql.GetPostgresSqlConnection(logger, *dbConnectionString, *dbMaxOpenConnection, *dbMaxIdleConnection, *verbosity)
	if err = ps.RunMigration(); err != nil {
		logger.Fatal(err)
	}
	var wg sync.WaitGroup
	wg.Add(*numberOfVendors)
	for i := 0; i < *numberOfVendors; i++ {
		go func(number int) {
			defer wg.Done()
			tempVendor := &dbModel.Vendor{
				Name: "vendor :" + strconv.Itoa(number)}
			ps.DB.Create(tempVendor)

			ps.DB.Create(&dbModel.Agent{
				Name: "agent :" + strconv.Itoa(number),
			})
			orderCount := random.Int31n(10)
			for i := 0; i < int(orderCount); i++ {
				durationTime := random.Intn(*maximumDelayTime) //maximum delivery time
				var tempTrip = &dbModel.Trip{
					Status: func() dbModel.TripStatusType {
						switch random.Intn(20) {
						case 0, 1, 2:
							return dbModel.ASSIGNED
						case 3:
							return dbModel.AtVendor
						case 4:
							return dbModel.PickedDelivered
						case 5, 6, 7, 8:
							return "DontCreate"
						case 9, 10, 11:
							return "UnKnownDelivered"
						default:
							return dbModel.DELIVERED

						}
					}(),
				}

				if tempTrip.Status != "DontCreate" {
					ps.DB.Create(&tempTrip)
				} else {
					tempTrip = nil
				}
				tempOrder := &dbModel.Order{
					VendorId:     tempVendor.ID,
					DeliveryTime: time.Minute * time.Duration(durationTime),
					Trip:         tempTrip,
				}
				ps.DB.Create(tempOrder)

			}
		}(i)

	}
	wg.Wait()

}
