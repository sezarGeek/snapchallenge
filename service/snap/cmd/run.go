package cmd

import (
	"github.com/go-resty/resty/v2"
	"github.com/namsral/flag"
	"github.com/sirupsen/logrus"
	"snapChallenge/api/handler"
	"snapChallenge/pkg/Queue"
	"snapChallenge/pkg/db"
)

var (
	dbConnectionString       = flag.String("db-connection-string", "host=localhost user=postgres password=3241550877 dbname=snap port=5432 sslmode=disable TimeZone=Asia/Tehran", "ConnectionString")
	verbosity                = flag.Int("verbosity", int(logrus.InfoLevel), "set verbosity level of the log")
	dbMaxIdleConnection      = flag.Int("db-max-idle-connection", 50, "DbMaxIdleConnection")
	dbMaxOpenConnection      = flag.Int("db-max-open-connection", 100, "DbMaxOpenConnection")
	applicationAddress       = flag.String("application-address", ":8000", "ApplicationAddress")
	dataBaseType             = flag.String("dataBase-type", "postgres", "DataBaseType")
	remainingTimeApi         = flag.String("remaining-time-api", "https://run.mocky.io/v3/5622a26a-c625-4b1f-a91d-71b312435f0e", "RemainingTimeApi")
	rabbitMqConnectionString = flag.String("rabbit-mq-connection-string", "amqp://test:test@localhost:5672/", "rabbitMqConnectionString")
	redisAddress             = flag.String("redis-address", "localhost:6379", "rabbitMqConnectionString")
	queueType                = flag.String("queue-type", "Redis", "queueType")
)

func Run() {
	flag.Parse()
	var queue Queue.Queue
	logger := logrus.New()
	dataBase, err := db.GetDataBaseConnection(logger, *dataBaseType, *dbConnectionString, *dbMaxOpenConnection, *dbMaxIdleConnection, *verbosity)
	if err != nil {
		logger.Fatal(err)
	}
	if err = dataBase.RunMigration(); err != nil {
		logger.Fatal(err)
	}
	if queue, err = Queue.StartQueueSystem(logger, *queueType, *rabbitMqConnectionString, *redisAddress); err != nil {
		logger.Fatal(err)

	}
	err = handler.StartRouter(logger, dataBase, resty.New(), queue, *remainingTimeApi, *applicationAddress)
	if err == nil {
		logger.Fatal(err)
	}
}
