# My Project

## Overview

This project is built using Golang 1.20, with Redis and PostgreSQL as its primary data storage solutions. It is designed
to be a robust, high-performance application, leveraging the strengths of Golang for backend development, Redis for
caching and real-time data processing, and PostgreSQL for relational database management.

## Table of Contents

- [Requirements](#requirements)
- [Installation](#installation)
- [Configuration_Local](#Configuration_Local)
  [Configuration_Docker_compose](#Configuration_Docker-compose)
- [Running the Application](#running-the-application)
- [Integration_test](#Integration_test)
- [Database Migrations](#database-migrations)
- [License](#license)

## Requirements

- Golang 1.20
- Redis
- PostgreSQL
- RabbitMQ

## Installation

### Golang

Ensure you have Go 1.20 installed. If not, you can download it from the [official Go website](https://golang.org/dl/).

### Redis

Install Redis by following the instructions on the [official Redis website](https://redis.io/download).

### PostgreSQL

Install PostgreSQL by following the instructions on
the [official PostgreSQL website](https://www.postgresql.org/download/).

### RabbitMQ

Install RabbitMQ by following the instructions on
the [official RabbitMQ website](https://www.rabbitmq.com/download.html).

## Configuration_Local

1. create dataBase like snap and get it as db-connection-string to my app. (you can see All Configuration on service/snap/cmd/Run)
2. Set Redis Connection ON redis-address
3. you can Generate Mock Data using cmd/mock-filler/main.go
4. you can Using Different Queue System like rabbitMq and Redis


## Configuration_Docker_compose
1. docker-compose run  it Generate All Service And Handle All 

## Integration_test
integration test exist in pkg/integration, and it contains various scenario of snap Delay-report-system
## Database Migrations

Database migrations are implemented to manage changes to the database schema over time. In this project, migrations are
handled automatically when the project starts.

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.

## delivery time is nanoSecond

## Run mock-filler to test