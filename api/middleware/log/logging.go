package log

import (
	"bytes"
	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"github.com/sirupsen/logrus"
)

type ginBodyLogger struct {
	// get all the methods implementation from the original one
	// override only the Write() method
	gin.ResponseWriter
	body bytes.Buffer
}

func RequestLoggingMiddleware(logger *logrus.Logger) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		ginBodyLogger := &ginBodyLogger{
			body:           bytes.Buffer{},
			ResponseWriter: ctx.Writer,
		}
		ctx.Writer = ginBodyLogger
		var req interface{}
		ctx.ShouldBindBodyWith(&req, binding.JSON)

		ctx.Next()
		logger.WithFields(logrus.Fields{
			"status":       ctx.Writer.Status(),
			"method":       ctx.Request.Method,
			"path":         ctx.Request.URL.Path,
			"query_params": ctx.Request.URL.Query(),
			"req_body":     req,
			"res_body":     ginBodyLogger.body.String(),
		}).Info("request details")
	}
}
