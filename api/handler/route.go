package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/go-resty/resty/v2"
	"github.com/sirupsen/logrus"
	v1 "snapChallenge/api/handler/v1"
	"snapChallenge/api/middleware/log"
	"snapChallenge/pkg/Queue"
	"snapChallenge/pkg/db"
)

func StartRouter(logger *logrus.Logger, dataBase db.DataBase, restyClient *resty.Client, queue Queue.Queue, remainingTimeApi, applicationAddress string) error {
	app := gin.New()
	app.Use(log.RequestLoggingMiddleware(logger))
	app.Use(gin.LoggerWithWriter(logger.Writer()))
	v1.SetUpRouters(app.Group("/v1"), dataBase, restyClient, queue, remainingTimeApi)

	return app.Run(applicationAddress)
}
