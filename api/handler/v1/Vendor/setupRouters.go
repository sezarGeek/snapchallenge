package vendor

import (
	"github.com/gin-gonic/gin"
	"snapChallenge/pkg/db"
)

func SetUpRouters(group *gin.RouterGroup, dataBase db.DataBase) {
	group.POST("/delay-report", delayReportHandler(dataBase))

}
