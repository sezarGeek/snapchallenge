package vendor

import (
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
	"net/http"
	"snapChallenge/api/handler/v1/Vendor/model"
	"snapChallenge/pkg/db"
	dbModel "snapChallenge/pkg/db/model"
	"time"
)

func delayReportHandler(dataBase db.DataBase) func(context *gin.Context) {

	var (
		VendorReq    model.VendorDelayRequest
		resultDelays []dbModel.Delay
		err          error
	)
	return func(context *gin.Context) {
		if err = context.ShouldBindBodyWithJSON(&VendorReq); err != nil {
			context.JSON(http.StatusBadRequest, model.VendorDelayResponse{
				ServiceResponse: model.ServiceResponse{IsSuccess: false, Message: err.Error()},
			})
			return
		}

		if resultDelays, err = dataBase.GetVendorDelay(dbModel.Vendor{
			Model: gorm.Model{ID: VendorReq.Vendor},
		}, time.Hour*24*7); err != nil {
			context.JSON(http.StatusOK, model.VendorDelayResponse{
				ServiceResponse: model.ServiceResponse{IsSuccess: false, Message: err.Error()},
			})
			return
		}

		var delayResponse []model.Delay
		for _, delay := range resultDelays {
			delayResponse = append(delayResponse, model.Delay{
				OrderID:      delay.OrderId,
				Delay:        dbModel.CustomDuration(delay.Time),
				DecisionType: string(delay.DecisionType),
				ReportedTime: delay.CreatedAt,
			})
		}
		context.JSON(http.StatusOK, model.VendorDelayResponse{Delays: delayResponse, VendorId: VendorReq.Vendor, ServiceResponse: model.ServiceResponse{
			IsSuccess: true,
		}})

	}
}
