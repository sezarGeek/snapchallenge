package model

import (
	"snapChallenge/pkg/db/model"
	"time"
)

type ServiceResponse struct {
	IsSuccess bool   `json:"is_success"`
	Message   string `json:"message"`
}

type RemainingTimeResponse struct {
	TimeDuration uint `json:"time_duration,omitempty"`
}

type Delay struct {
	OrderID      uint                 `json:"order_id"` // Foreign key
	Delay        model.CustomDuration `json:"delay,omitempty"`
	DecisionType string               `json:"decision-type,omitempty"`
	ReportedTime time.Time            `json:"reported_time,omitempty"`
}

type VendorDelayResponse struct {
	ServiceResponse
	VendorId uint    `json:"vendor_id,omitempty"`
	Delays   []Delay `json:"delays,omitempty"`
}
