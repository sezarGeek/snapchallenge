package model

type VendorDelayRequest struct {
	Vendor uint `json:"vendor_id"`
}
