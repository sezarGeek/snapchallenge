package model

import (
	"snapChallenge/pkg/db/model"
)

type ServiceResponse struct {
	IsSuccess bool   `json:"is_success"`
	Message   string `json:"message,omitempty"`
}

type GetOrderDelayQueueResponse struct {
	ServiceResponse
	OrderId          uint                 `json:"order_id,,omitempty"`
	VendorID         uint                 `json:"vendor_id,,omitempty"`
	TripId           uint                 `json:"trip_id,,omitempty"`
	UpdateTripStatus model.TripStatusType `json:"update_trip_status,,omitempty"`
}
