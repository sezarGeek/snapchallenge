package employeee

import (
	"github.com/gin-gonic/gin"
	"snapChallenge/pkg/Queue"
	"snapChallenge/pkg/db"
)

func SetUpRouters(group *gin.RouterGroup, dataBase db.DataBase, queue Queue.Queue) {
	group.GET("/check-order", getDelayHandler(dataBase, queue))

}
