package employeee

import (
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
	"net/http"
	"snapChallenge/api/handler/v1/Employee/model"
	"snapChallenge/pkg/db"
	dbModel "snapChallenge/pkg/db/model"

	"snapChallenge/pkg/Queue"
)

func getDelayHandler(dataBase db.DataBase, queue Queue.Queue) func(context *gin.Context) {
	return func(context *gin.Context) {

		var order dbModel.Order
		var orderId uint
		var err error
		var trip dbModel.Trip
		if orderId, err = queue.ConsumeDelay(); err != nil {
			context.JSON(http.StatusInternalServerError, model.GetOrderDelayQueueResponse{
				ServiceResponse: model.ServiceResponse{
					IsSuccess: false,
					Message:   err.Error(),
				},
			})
			return
		}
		if order, err = dataBase.GetOrderDetail(dbModel.Order{
			Model: gorm.Model{
				ID: orderId,
			},
		}); err != nil {
			context.JSON(http.StatusBadRequest, model.GetOrderDelayQueueResponse{
				ServiceResponse: model.ServiceResponse{
					IsSuccess: false,
					Message:   err.Error(),
				},
			})
			return

		} else if order.ID == 0 {
			context.JSON(http.StatusBadRequest, model.GetOrderDelayQueueResponse{
				ServiceResponse: model.ServiceResponse{
					IsSuccess: false,
					Message:   "bad request",
				},
			})
			return

		}

		if trip, err = dataBase.CreateOrUpdateTrip(dbModel.Order{Model: gorm.Model{ID: orderId}}, dbModel.ASSIGNED); err != nil {
			context.JSON(http.StatusCreated, model.GetOrderDelayQueueResponse{
				ServiceResponse: model.ServiceResponse{
					IsSuccess: false,
					Message:   err.Error(),
				},
			})
			return
		}

		context.JSON(http.StatusOK, model.GetOrderDelayQueueResponse{
			ServiceResponse: model.ServiceResponse{
				IsSuccess: true,
			},
			OrderId:          order.ID,
			VendorID:         order.VendorId,
			TripId:           trip.ID,
			UpdateTripStatus: trip.Status,
		})
		return
	}
}
