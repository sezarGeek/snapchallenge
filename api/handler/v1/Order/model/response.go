package model

import (
	"snapChallenge/pkg/db/model"
)

type ServiceResponse struct {
	IsSuccess bool   `json:"is_success"`
	Message   string `json:"message,omitempty"`
}

type RemainingTimeResponse struct {
	TimeDuration uint `json:"time_duration"`
}

type OrderDelayResponse struct {
	ServiceResponse
	OrderId          uint                 `json:"order_id"`
	NewDeliveryTime  string               `json:"new_delivery_time"`
	TripStatus       model.TripStatusType `json:"trip-status"`
	OverallDelayTime model.CustomDuration `json:"overall_delay_time"`
}
