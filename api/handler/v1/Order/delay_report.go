package order

import (
	"github.com/gin-gonic/gin"
	"github.com/go-resty/resty/v2"
	"gorm.io/gorm"
	"net/http"
	"snapChallenge/api/handler/v1/Order/model"
	"snapChallenge/pkg/Queue"
	"snapChallenge/pkg/db"
	dbModel "snapChallenge/pkg/db/model"
	"time"
)

func delayReportHandler(dataBase db.DataBase, restyClient *resty.Client, queue Queue.Queue, remainingTimeApi string) func(context *gin.Context) {
	return func(context *gin.Context) {
		var (
			orderReq              model.OrderDelayRequest
			err                   error
			RemainingTimeResponse model.RemainingTimeResponse
			order                 dbModel.Order
		)
		if err = context.ShouldBindBodyWithJSON(&orderReq); err != nil {
			context.JSON(http.StatusBadRequest, model.OrderDelayResponse{
				ServiceResponse: model.ServiceResponse{IsSuccess: false, Message: err.Error()},
			})
			return
		}

		if order, err = dataBase.GetOrderDetail(dbModel.Order{
			Model: gorm.Model{ID: orderReq.OrderId},
		}); err != nil {
			context.JSON(http.StatusNotFound, model.OrderDelayResponse{
				ServiceResponse: model.ServiceResponse{IsSuccess: false, Message: err.Error()},
			})
			return
		}
		if order.ID == 0 {
			context.JSON(http.StatusNotFound, model.OrderDelayResponse{
				ServiceResponse: model.ServiceResponse{IsSuccess: false, Message: "Not Found order"},
			})
			return
		}

		//order exist
		if time.Now().Sub(order.CreatedAt) < order.DeliveryTime {
			context.JSON(http.StatusOK, model.OrderDelayResponse{
				ServiceResponse: model.ServiceResponse{IsSuccess: true},
				OrderId:         order.ID,
				NewDeliveryTime: order.CreatedAt.Add(order.DeliveryTime).Format(time.RFC3339Nano),
				TripStatus: func(trip *dbModel.Trip) dbModel.TripStatusType {
					if trip == nil {
						return ""
					}
					return trip.Status
				}(order.Trip),
				OverallDelayTime: dbModel.CustomDuration(order.DeliveryTime),
			})
			return
		}
		if order.Trip == nil || (order.Trip.Status != dbModel.ASSIGNED && order.Trip.Status != dbModel.PickedDelivered && order.Trip.Status != dbModel.AtVendor) {
			if err = queue.PublishToDelay(order.ID); err != nil {
				context.JSON(http.StatusInternalServerError, model.OrderDelayResponse{
					ServiceResponse: model.ServiceResponse{IsSuccess: false, Message: err.Error()},
				})
				return
			}
			if err = dataBase.SetDelays(dbModel.Delay{DecisionType: dbModel.AddToDelayProcess, OrderId: order.ID}); err != nil {
				context.JSON(http.StatusInternalServerError, model.OrderDelayResponse{
					ServiceResponse: model.ServiceResponse{IsSuccess: false, Message: err.Error()},
				})
				return
			}

			context.JSON(http.StatusOK, model.OrderDelayResponse{
				ServiceResponse: model.ServiceResponse{IsSuccess: true, Message: "Your Order Process as soon as possible"},
				OrderId:         order.ID,
			})
			return

		} else {
			if _, err = restyClient.R().SetResult(&RemainingTimeResponse).Get(remainingTimeApi); err != nil {
				context.JSON(http.StatusInternalServerError, model.OrderDelayResponse{
					ServiceResponse: model.ServiceResponse{IsSuccess: false, Message: err.Error()},
					OrderId:         order.ID,
				})
			}
			currentDelay := time.Duration(RemainingTimeResponse.TimeDuration) * time.Minute
			prevDelay := time.Now().Sub(order.CreatedAt.Add(order.DeliveryTime)) // DeleivaeryTime - now -->Delivert time is time that Report to Customer in previuos step
			if err = dataBase.SetDelays(dbModel.Delay{Time: currentDelay + prevDelay, DecisionType: dbModel.AddTimeDelayDecisionType, OrderId: order.ID}); err != nil {
				context.JSON(http.StatusInternalServerError, model.OrderDelayResponse{
					ServiceResponse: model.ServiceResponse{IsSuccess: false, Message: err.Error()},
					OrderId:         order.ID,
				})
			}
			context.JSON(http.StatusOK, model.OrderDelayResponse{
				ServiceResponse:  model.ServiceResponse{IsSuccess: true},
				OrderId:          order.ID,
				NewDeliveryTime:  time.Now().Add(currentDelay).Format(time.RFC3339Nano),
				TripStatus:       order.Trip.Status,
				OverallDelayTime: dbModel.CustomDuration(order.DeliveryTime + currentDelay),
			})
			return
		}

	}
}
