package order

import (
	"github.com/gin-gonic/gin"
	"github.com/go-resty/resty/v2"
	"snapChallenge/pkg/Queue"
	"snapChallenge/pkg/db"
)

func SetUpRouters(group *gin.RouterGroup, dataBase db.DataBase, restyClient *resty.Client, queue Queue.Queue, remainingTimeApi string) {
	group.POST("/delay-report", delayReportHandler(dataBase, restyClient, queue, remainingTimeApi))

}
