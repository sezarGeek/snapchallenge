package v1

import (
	"github.com/gin-gonic/gin"
	"github.com/go-resty/resty/v2"
	employeee "snapChallenge/api/handler/v1/Employee"
	order "snapChallenge/api/handler/v1/Order"
	vendor "snapChallenge/api/handler/v1/Vendor"
	"snapChallenge/pkg/db"

	"snapChallenge/pkg/Queue"
)

func SetUpRouters(group *gin.RouterGroup, dataBase db.DataBase, restyClient *resty.Client, queue Queue.Queue, remainingTimeApi string) {
	employeee.SetUpRouters(group.Group("/employee"), dataBase, queue)
	order.SetUpRouters(group.Group("/order"), dataBase, restyClient, queue, remainingTimeApi)
	vendor.SetUpRouters(group.Group("/vendor"), dataBase)

}
