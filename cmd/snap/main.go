package main

import (
	"snapChallenge/service/snap/cmd"
)

func main() {
	cmd.Run()
}
