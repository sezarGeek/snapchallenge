# Start from the official Golang base image
FROM golang:1.20-alpine

# Install dependencies
RUN apk add --no-cache git

# Set arguments and environment variables
ARG DB_CONNECTION_STRING
ENV DB_CONNECTION_STRING=$DB_CONNECTION_STRING

ARG VERBOSITY
ENV VERBOSITY=$VERBOSITY

ARG DB_MAX_IDLE_CONNECTION
ENV DB_MAX_IDLE_CONNECTION=$DB_MAX_IDLE_CONNECTION

ARG DB_MAX_OPEN_CONNECTION
ENV DB_MAX_OPEN_CONNECTION=$DB_MAX_OPEN_CONNECTION

ARG APPLICATION_ADDRESS
ENV APPLICATION_ADDRESS=$APPLICATION_ADDRESS

ARG DATABASE_TYPE
ENV DATABASE_TYPE=$DATABASE_TYPE

ARG REMAINING_TIME_API
ENV REMAINING_TIME_API=$REMAINING_TIME_API

ARG RABBIT_MQ_CONNECTION_STRING
ENV RABBIT_MQ_CONNECTION_STRING=$RABBIT_MQ_CONNECTION_STRING

ARG QUEUE_TYPE
ENV QUEUE_TYPE=$QUEUE_TYPE

ARG REDIS_ADDRESS
ENV REDIS_ADDRESS=$REDIS_ADDRESS

# Set the Current Working Directory inside the container
WORKDIR /app

RUN export GOROOT="$GOROOT:/app"


# Copy the rest of the source code
COPY . .

# Download and cache dependencies
RUN go mod download

# Run go mod tidy to ensure all modules are in place
RUN go mod tidy

RUN ls
# Build the Go app
RUN go build -o snap ./cmd/snap/main.go

# Command to run the executable
CMD ["./snap"]
