package Queue

import (
	"errors"
	"github.com/sirupsen/logrus"
	"snapChallenge/pkg/Queue/rabbit"
	"snapChallenge/pkg/Queue/redis"
)

type Queue interface {
	PublishToDelay(orderId uint) error
	ConsumeDelay() (orderId uint, err error)
}

func StartQueueSystem(logger *logrus.Logger, queueType, rabbitMqConnectionString, redisConnectionString string) (Queue, error) {
	switch queueType {
	case redis.RedisName:
		return redis.InitRedis(logger, redisConnectionString), nil
	case rabbit.RabbitMQName:
		return rabbit.InitQueue(logger, rabbitMqConnectionString), nil
	default:
		return nil, errors.New("Invalid Q Type")
	}
}
