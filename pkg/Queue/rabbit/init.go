package rabbit

import (
	amqp "github.com/rabbitmq/amqp091-go"
	"github.com/sirupsen/logrus"
)

type RabbitMQ struct {
	*amqp.Connection
	logger *logrus.Logger
}

func failOnError(err error, msg string, logger *logrus.Logger) {
	if err != nil {
		logger.Fatalf("%s: %s", msg, err)
	}
}

const RabbitMQName = "RabbitMQ"
const DelayQueueName = "DelayQueue"

func InitQueue(logger *logrus.Logger, rabbitMqConnectionString string) RabbitMQ {
	conn, err := amqp.Dial(rabbitMqConnectionString)
	failOnError(err, "Failed to connect to RabbitMQ", logger)
	ch, err := conn.Channel()
	failOnError(err, "Failed to open a channel", logger)
	_, err = ch.QueueDeclare(
		DelayQueueName, // queue name
		true,           // durable
		false,          // delete when unused
		false,          // exclusive
		false,          // no-wait
		nil,            // arguments
	)
	failOnError(err, "Failed to declare a queue", logger)

	return RabbitMQ{
		Connection: conn,
		logger:     logger,
	}

}
