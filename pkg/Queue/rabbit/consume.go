package rabbit

import (
	"errors"
	amqp "github.com/rabbitmq/amqp091-go"
	"strconv"
	"time"
)

func (r RabbitMQ) ConsumeDelay() (orderId uint, err error) {
	var ch *amqp.Channel
	defer func() {
		r.IsClosed()
	}()
	if ch, err = r.Channel(); err != nil {
		return
	}
	defer ch.Close()

	var intOrderId int
	msg, err := ch.Consume(DelayQueueName, "Employee", true, true, true, true, nil)
	if err != nil {

		return 0, err
	}
	select {
	case data := <-msg:
		intOrderId, err = strconv.Atoi(string(data.Body))
	case <-time.After(3 * time.Second):
		err = errors.New("Not Exists Order to Proccess")
	}

	return uint(intOrderId), err
}
