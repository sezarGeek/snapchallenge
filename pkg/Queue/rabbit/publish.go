package rabbit

import (
	amqp "github.com/rabbitmq/amqp091-go"
	"strconv"
)

func (r RabbitMQ) PublishToDelay(orderId uint) (err error) {
	var ch *amqp.Channel
	defer func() {
		ch.Close()
	}()
	if ch, err = r.Channel(); err != nil {
		return
	}

	return ch.Publish(
		"",             // exchange
		DelayQueueName, // routing key
		false,          // mandatory
		false,          // immediate
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        []byte(strconv.Itoa(int(orderId))),
		})
}
