package redis

import (
	"github.com/go-redis/redis/v8"
	"github.com/sirupsen/logrus"
)

type RedisClient struct {
	*redis.Client
	logger *logrus.Logger
}

const RedisName = "Redis"
const DelayQueueName = "DelayQueue"

func InitRedis(logger *logrus.Logger, redisConnectionString string) RedisClient {
	client := redis.NewClient(&redis.Options{
		Addr: redisConnectionString,
	})
	return RedisClient{
		Client: client,
		logger: logger,
	}
}
