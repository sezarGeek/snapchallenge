package redis

import (
	"context"
	"errors"
	"strconv"
)

func (r RedisClient) ConsumeDelay() (orderId uint, err error) {
	defer func() {
		r.Del(context.Background(), DelayQueueName+strconv.Itoa(int(orderId)))
	}()
	value, err := r.RPopCount(context.Background(), DelayQueueName, 1).Result()
	if err != nil {
		return 0, err
	}
	if len(value) == 0 {
		return 0, errors.New("Queue is empty")
	}
	intValue, err := strconv.Atoi(value[0])
	return uint(intValue), err
}
