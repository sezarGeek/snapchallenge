package redis

import (
	"context"
	"errors"
	"strconv"
)

func (r RedisClient) PublishToDelay(orderId uint) (err error) {
	status := r.SetNX(context.Background(), DelayQueueName+strconv.Itoa(int(orderId)), true, 0)
	if !status.Val() {
		return errors.New("this order_id is already exist")
	}
	r.RPush(context.Background(), DelayQueueName, orderId)
	return nil
}
