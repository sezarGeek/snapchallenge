package model

import (
	"time"
)

const ( //Trip Status

	ASSIGNED        TripStatusType = "ASSIGNED"
	AtVendor        TripStatusType = "AtVendor"
	PickedDelivered TripStatusType = "PickedDelivered"
	DELIVERED       TripStatusType = "DELIVERED"
)
const (
	AddTimeDelayDecisionType DelayDecisionType = "AddTime"
	AddToDelayProcess        DelayDecisionType = "AddToDelayProcess"
)

const DefaultDeliveryTime = time.Minute * 10
