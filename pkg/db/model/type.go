package model

import (
	"encoding/json"
	"fmt"
	"time"
)

type TripStatusType string

type CustomDuration time.Duration

func (c CustomDuration) MarshalJSON() ([]byte, error) {

	outPut := fmt.Sprintf("%f min", time.Duration(c).Minutes())
	return json.Marshal(outPut)
}
