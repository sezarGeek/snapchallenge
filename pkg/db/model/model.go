package model

import (
	"gorm.io/gorm"
	"time"
)

type Vendor struct {
	gorm.Model
	Name   string  `gorm:"type:varchar(32)"`
	Orders []Order `gorm:"foreignKey:VendorId"` // One-to-Many: Vendor has many Orders
}
type Agent struct {
	gorm.Model
	Name string `gorm:"type:varchar(32)"`
}

type Order struct {
	gorm.Model
	VendorId     uint // Belongs To: Order belongs to one Vendor
	DeliveryTime time.Duration
	Delays       []Delay `gorm:"foreignKey:OrderId"` // One-to-Many: Order has many Delays
	Trip         *Trip   `gorm:"constraint:OnUpdate:CASCADE,OnDelete:SET NULL;"`
}

type Trip struct {
	gorm.Model
	OrderID uint `gorm:"unique;not null"`
	Order   *Order
	Status  TripStatusType // Assuming TripStatusType is defined elsewhere

}
type DelayDecisionType string

type Delay struct {
	gorm.Model
	OrderId      uint // Belongs To: Delay belongs to one Order
	Time         time.Duration
	DecisionType DelayDecisionType `json:"decision-type" gorm:"type:varchar(32)"`
}
