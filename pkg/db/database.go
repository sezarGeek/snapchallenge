package db

import (
	"errors"
	"github.com/sirupsen/logrus"
	"snapChallenge/pkg/db/model"
	"snapChallenge/pkg/db/postgresql"
	"time"
)

type DataBase interface {
	RunMigration() error
	GetOrderDetail(order model.Order) (resultOrders model.Order, err error)
	GetVendorDelay(order model.Vendor, periodTime time.Duration) (resultDelay []model.Delay, err error)
	SetDelays(delay model.Delay) (err error)
	CreateOrUpdateTrip(order model.Order, status model.TripStatusType) (trip model.Trip, err error)
}

func GetDataBaseConnection(logger *logrus.Logger, dbType, dbConnectionString string, dbMaxOpenConnection, dbMaxIdleConnection, verbosity int) (DataBase, error) {
	switch dbType {
	case postgresql.DbTyp:
		return postgresql.GetPostgresSqlConnection(logger, dbConnectionString, dbMaxOpenConnection, dbMaxIdleConnection, verbosity)

	default:
		return nil, errors.New("Invalid DataBaseType :" + dbType)
	}
}
