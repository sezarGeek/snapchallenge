package postgresql

import (
	"context"
	"github.com/sirupsen/logrus"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	gormLogger "gorm.io/gorm/logger"
)

const DbTyp = "postgres"

func GetPostgresSqlConnection(logger *logrus.Logger, dbConnectionString string, dbMaxOpenConnection, dbMaxIdleConnection, verbosity int) (postgresql PostgresSql, err error) {
	gormLoggerLevel := setUpGormLevelLog(verbosity)
	if postgresql.DB, err = gorm.Open(postgres.Open(dbConnectionString), &gorm.Config{
		Logger: gormLogger.Default.LogMode(gormLoggerLevel),
	}); err != nil {
		logger.Error(context.TODO(), "open gorm failed", dbConnectionString, nil, err)
		return
	}

	if postgresql.SqlDb, err = postgresql.DB.DB(); err != nil {
		logger.Error(context.TODO(), "Get gorm failed", nil, nil, err)
		return
	}
	postgresql.SqlDb.SetMaxOpenConns(dbMaxOpenConnection)
	postgresql.SqlDb.SetMaxIdleConns(dbMaxIdleConnection)

	return
}
