package postgresql

import (
	"context"
	"database/sql"
	gormigrate "github.com/go-gormigrate/gormigrate/v2"
	"github.com/sirupsen/logrus"
	"gorm.io/gorm"
	gormLogger "gorm.io/gorm/logger"
	"snapChallenge/pkg/db/model"
)

type PostgresSql struct {
	DB     *gorm.DB
	SqlDb  *sql.DB
	Logger *logrus.Logger
}

func (p PostgresSql) RunMigration() (err error) {
	migration := gormigrate.New(p.DB, gormigrate.DefaultOptions, []*gormigrate.Migration{
		// todo add migration Here

	})
	migration.InitSchema(func(tx *gorm.DB) (err error) {
		err = tx.AutoMigrate(
			&model.Vendor{},
			&model.Agent{},
			&model.Trip{},
			&model.Delay{},
			&model.Order{},
		)
		return err

	})
	if err = migration.Migrate(); err != nil {
		p.Logger.Error(context.TODO(), "could not migrate", nil, nil, err)
		return err
	}
	return err
}
func setUpGormLevelLog(verbosity int) gormLogger.LogLevel {
	switch logrus.Level(verbosity) {
	case logrus.InfoLevel:
		return gormLogger.Info
	case logrus.WarnLevel:
		return gormLogger.Warn
	case logrus.ErrorLevel:
		return gormLogger.Silent
	default:
		return gormLogger.Info
	}
}
