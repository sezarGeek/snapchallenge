package postgresql

import (
	"snapChallenge/pkg/db/model"
	"sort"
	"time"
)

func (p PostgresSql) GetVendorDelay(vendor model.Vendor, periodTime time.Duration) (resultOrders []model.Delay, err error) {
	var result []model.Order
	err = p.DB.Debug().Preload("Delays").Where("orders.created_at > ?", time.Now().Add(-1*periodTime)).Find(&result, model.Order{VendorId: vendor.ID}).Error
	for _, order := range result {
		resultOrders = append(resultOrders, order.Delays...)
	}
	sort.Slice(resultOrders, func(i, j int) bool {
		return resultOrders[i].CreatedAt.After(resultOrders[j].CreatedAt)
	})

	return
}
