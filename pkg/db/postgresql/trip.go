package postgresql

import (
	"gorm.io/gorm"
	dbModel "snapChallenge/pkg/db/model"
)

func (p PostgresSql) CreateOrUpdateTrip(order dbModel.Order, status dbModel.TripStatusType) (trip dbModel.Trip, err error) {
	var resultOrder dbModel.Order
	err = p.DB.Preload("Trip").First(&resultOrder, order).Error
	if err != nil {
		return
	}

	if resultOrder.Trip == nil || resultOrder.Trip.ID == 0 {
		if err = p.DB.Create(&dbModel.Trip{
			Status: status,
		}).First(&trip).Error; err != nil {
			return
		}
		order.Trip.ID = trip.ID
		p.DB.Where(dbModel.Order{Model: gorm.Model{ID: order.ID}}).Updates(order)
	} else {
		if err = p.DB.Where(dbModel.Trip{Model: gorm.Model{ID: resultOrder.Trip.ID}}).Updates(
			&dbModel.Trip{
				Status: dbModel.ASSIGNED,
			}).First(&trip).Error; err != nil {
			return
		}
	}

	return
}
