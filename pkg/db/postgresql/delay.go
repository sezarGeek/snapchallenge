package postgresql

import (
	"fmt"
	"snapChallenge/pkg/db/model"
)

func (p PostgresSql) SetDelays(delay model.Delay) (err error) {
	err = p.DB.Create(&delay).Error
	if err != nil {
		p.Logger.Errorf(fmt.Sprintf("Error in Get Order data input: %v  error: %e", delay, err))
	}
	return
}
