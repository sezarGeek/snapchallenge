package postgresql

import (
	"snapChallenge/pkg/db/model"
)

func (p PostgresSql) GetOrderDetail(order model.Order) (resultOrder model.Order, err error) {
	err = p.DB.Debug().Model(model.Order{}).
		Joins("LEFT JOIN trips ON trips.order_id = orders.id").
		Joins("LEFT JOIN delays ON delays.order_id = orders.id").
		Preload("Trip").Preload("Delays").First(&resultOrder, &order).Error
	for _, delay := range resultOrder.Delays {
		resultOrder.DeliveryTime += delay.Time
	}

	return
}
