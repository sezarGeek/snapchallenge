package test

import (
	model2 "snapChallenge/pkg/db/model"
)

type ServiceResponse struct {
	IsSuccess bool   `json:"is_success"`
	Message   string `json:"message,omitempty"`
}

type OrderDelayResponse struct {
	ServiceResponse
	OrderId          uint                  `json:"order_id"`
	NewDeliveryTime  string                `json:"new_delivery_time"`
	TripStatus       model2.TripStatusType `json:"trip-status"`
	OverallDelayTime string                `json:"overall_delay_time"`
}

type VendorDelayResponse struct {
	ServiceResponse
	VendorId uint    `json:"vendor_id,omitempty"`
	Delays   []Delay `json:"delays,omitempty"`
}

type GetOrderDelayQueueResponse struct {
	ServiceResponse
	OrderId           uint   `json:"order_id"`
	VendorId          uint   `json:"vendor_id"`
	TripId            uint   `json:"trip_id"`
	UpdatedTripStatus string `json:"update_trip_status"`
}

type Delay struct {
	OrderID      uint                  // Foreign key
	Time         model2.CustomDuration `json:"time"`
	DecisionType string                `json:"decision-type"`
}
