package test

import "github.com/go-resty/resty/v2"

const (
	CheckOrderUrl      = "http://localhost:8000/v1/employee/check-order"
	VendorDelay        = "http://localhost:8000/v1/vendor/delay-report"
	OrderDelayReport   = "http://localhost:8000/v1/order/delay-report"
	dbConnectionString = "host=localhost user=postgres password=3241550877 dbname=snap port=5432 sslmode=disable TimeZone=Asia/Tehran"
)

func SendOrderDelayRequest(restyClient *resty.Client, orderId int) (resp OrderDelayResponse, err error) {
	_, err = restyClient.R().SetResult(&resp).SetBody(map[string]interface{}{
		"order_id": orderId,
	}).Post(OrderDelayReport)
	return
}
func SenVendorDelayRequest(restyClient *resty.Client, venoderId int) (resp VendorDelayResponse, err error) {
	_, err = restyClient.R().SetResult(&resp).SetBody(map[string]interface{}{
		"vendor_id": venoderId,
	}).Post(VendorDelay)
	return
}
func SendCheckOrderRequest(restyClient *resty.Client) (resp GetOrderDelayQueueResponse, err error) {
	_, err = restyClient.R().SetResult(&resp).Get(CheckOrderUrl)
	return
}
