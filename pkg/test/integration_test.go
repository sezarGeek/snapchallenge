package test

import (
	"encoding/json"
	"github.com/go-resty/resty/v2"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"snapChallenge/pkg/db/model"
	"testing"
	"time"
)

var (
	restyClient *resty.Client
	dataBase    *gorm.DB
	err         error
)

func TestMain(m *testing.M) {
	restyClient = resty.New()
	if dataBase, err = gorm.Open(postgres.Open(dbConnectionString)); err != nil {
		return
	}
	m.Run()
}
func TestDelayReportNotExist(t *testing.T) {
	if resp, err := SendOrderDelayRequest(restyClient, -1); err != nil || resp.IsSuccess {
		t.Errorf(err.Error(), " response most be notSucces ")
	}
}
func TestDelayReportThatNotDelayedReally(t *testing.T) {
	///Create Model and insert to Database
	var delayByte []byte
	vendor := model.Vendor{
		Name: "TestVenoder",
	}

	dataBase.Create(&vendor)
	defer dataBase.Where(&vendor).Delete(&vendor)

	order := model.Order{
		VendorId:     vendor.ID,
		DeliveryTime: 10 * time.Hour,
	}

	dataBase.Create(&order)
	defer dataBase.Where(&order).Delete(&order)

	trip := model.Trip{
		OrderID: order.ID,
		Status:  model.ASSIGNED,
	}
	dataBase.Create(&trip)
	defer dataBase.Where(&trip).Delete(&trip)

	///Create Test

	resp, err := SendOrderDelayRequest(restyClient, int(order.ID))

	if err != nil {
		t.Errorf(err.Error(), " response most be notSucces ")
	}
	delayByte, err = json.Marshal(model.CustomDuration(10 * time.Hour))
	if err != nil {
		t.Errorf(err.Error(), " response most be notSucces ")
	}
	if !resp.IsSuccess {
		t.Errorf("!resp.IsSuccess  = %v", !resp.IsSuccess)
	}
	if resp.OrderId != order.ID {
		t.Errorf("  resp.OrderId != order.ID %v !=%v", resp.OrderId, order.ID)
	}
	if resp.TripStatus != model.ASSIGNED {
		t.Errorf(" resp.TripStatus != model.ASSIGNED %v != %v ", resp.TripStatus, model.ASSIGNED)
	}
	if string(delayByte[1:len(delayByte)-1]) != resp.OverallDelayTime {
		t.Errorf("  string(delayByte[1:len(delayByte)-1]) != resp.OverallDelayTime  %v != %v ", string(delayByte[1:len(delayByte)-1]), resp.OverallDelayTime)
	}

}
func TestDelayReportThatDelayedAndDidntHaveAnyTrip(t *testing.T) {
	///Create Model and insert to Database
	vendor := model.Vendor{
		Name: "TestVenoder",
	}

	dataBase.Create(&vendor)
	defer dataBase.Where(&vendor).Delete(&vendor)

	order := model.Order{
		VendorId:     vendor.ID,
		DeliveryTime: 10 * time.Microsecond,
	}

	dataBase.Create(&order)
	defer dataBase.Where(&order).Delete(&order)

	///Create Test
	time.Sleep(time.Second)
	resp, err := SendOrderDelayRequest(restyClient, int(order.ID))

	if err != nil {
		t.Errorf(err.Error(), " response most be notSucces ")
	}
	if err != nil {
		t.Errorf(err.Error(), " response most be notSucces ")
	}
	if !resp.IsSuccess {
		t.Errorf("!resp.IsSuccess  = %v", !resp.IsSuccess)
	}
	if resp.OrderId != order.ID {
		t.Errorf("  resp.OrderId != order.ID %v !=%v", resp.OrderId, order.ID)
	}
	if resp.TripStatus != "" {
		t.Errorf(" resp.TripStatus != model.ASSIGNED %v != %v ", resp.TripStatus, model.ASSIGNED)
	}

}
func TestDelayReportThatDelayedAndHaveBadTripStatus(t *testing.T) {
	///Create Model and insert to Database
	vendor := model.Vendor{
		Name: "TestVenoder",
	}

	dataBase.Create(&vendor)
	defer dataBase.Where(&vendor).Delete(&vendor)

	order := model.Order{
		VendorId:     vendor.ID,
		DeliveryTime: 10 * time.Microsecond,
	}

	dataBase.Create(&order)
	defer dataBase.Where(&order).Delete(&order)

	trip := model.Trip{
		OrderID: order.ID,
		Status:  "BadTrip",
	}
	dataBase.Create(&trip)
	defer dataBase.Where(&trip).Delete(&trip)

	///Create Test
	time.Sleep(time.Second)
	resp, err := SendOrderDelayRequest(restyClient, int(order.ID))

	if err != nil {
		t.Errorf(err.Error(), " response most be notSucces ")
	}
	if err != nil {
		t.Errorf(err.Error(), " response most be notSucces ")
	}
	if !resp.IsSuccess {
		t.Errorf("!resp.IsSuccess  = %v", !resp.IsSuccess)
	}
	if resp.OrderId != order.ID {
		t.Errorf("  resp.OrderId != order.ID %v !=%v", resp.OrderId, order.ID)
	}
	if resp.TripStatus != "" {
		t.Errorf(" resp.TripStatus != model.ASSIGNED %v != %v ", resp.TripStatus, model.ASSIGNED)
	}

}
func TestDelayReportThatDelayedAndHaveTripAndContuieTripsByCallingOrder(t *testing.T) {
	///Create Model and insert to Database
	var delayByte []byte
	vendor := model.Vendor{
		Name: "TestVenoder",
	}

	dataBase.Create(&vendor)
	defer dataBase.Where(&vendor).Delete(&vendor)

	order := model.Order{
		VendorId:     vendor.ID,
		DeliveryTime: time.Nanosecond,
	}

	dataBase.Create(&order)
	defer dataBase.Where(&order).Delete(&order)

	trip := model.Trip{
		OrderID: order.ID,
		Status:  model.ASSIGNED,
	}
	dataBase.Create(&trip)
	defer dataBase.Where(&trip).Delete(&trip)

	///Create Test

	time.Sleep(time.Microsecond)
	resp, err := SendOrderDelayRequest(restyClient, int(order.ID))

	if err != nil {
		t.Errorf(err.Error(), " response most be notSucces ")
	}
	delayByte, err = json.Marshal(model.CustomDuration(5 * time.Minute))
	if err != nil {
		t.Errorf(err.Error(), " response most be notSucces ")
	}
	if !resp.IsSuccess {
		t.Errorf("!resp.IsSuccess  = %v", !resp.IsSuccess)
	}
	if resp.OrderId != order.ID {
		t.Errorf("  resp.OrderId != order.ID %v !=%v", resp.OrderId, order.ID)
	}
	if resp.TripStatus != model.ASSIGNED {
		t.Errorf(" resp.TripStatus != model.ASSIGNED %v != %v ", resp.TripStatus, model.ASSIGNED)
	}
	if string(delayByte[1:len(delayByte)-1]) != resp.OverallDelayTime {
		t.Errorf("  string(delayByte[1:len(delayByte)-1]) != resp.OverallDelayTime  %v != %v ", string(delayByte[1:len(delayByte)-1]), resp.OverallDelayTime)
	}
}
func TestDelayReportDeliveredTripAndAgentProcessIt(t *testing.T) {
	///Create Model and insert to Database
	var delayByte []byte
	vendor := model.Vendor{
		Name: "TestVenoder",
	}

	dataBase.Create(&vendor)
	defer dataBase.Where(&vendor).Delete(&vendor)

	order := model.Order{
		VendorId:     vendor.ID,
		DeliveryTime: time.Nanosecond,
	}

	dataBase.Create(&order)
	defer dataBase.Where(&order).Delete(&order)

	trip := model.Trip{
		OrderID: order.ID,
		Status:  model.DELIVERED,
	}
	dataBase.Create(&trip)
	defer dataBase.Where(&trip).Delete(&trip)

	///Create Test

	time.Sleep(time.Microsecond)
	resp, err := SendOrderDelayRequest(restyClient, int(order.ID))

	if err != nil {
		t.Errorf(err.Error(), " response most be notSucces ")
	}
	delayByte, err = json.Marshal(model.CustomDuration(0))
	if err != nil {
		t.Errorf(err.Error(), " response most be notSucces ")
	}
	if !resp.IsSuccess {
		t.Errorf("!resp.IsSuccess  = %v", !resp.IsSuccess)
	}
	if resp.OrderId != order.ID {
		t.Errorf("  resp.OrderId != order.ID %v !=%v", resp.OrderId, order.ID)
	}
	if resp.TripStatus != "" {
		t.Errorf(" resp.TripStatus !=null %v ", resp.TripStatus)
	}
	if string(delayByte[1:len(delayByte)-1]) != resp.OverallDelayTime {
		t.Errorf("  string(delayByte[1:len(delayByte)-1]) != resp.OverallDelayTime  %v != %v ", string(delayByte[1:len(delayByte)-1]), resp.OverallDelayTime)
	}

	rsp, err := SendCheckOrderRequest(restyClient)
	if err != nil {
		t.Errorf(err.Error(), " response most be notSucces ")
	}

	if !rsp.IsSuccess {
		t.Errorf(" !rsp.IsSuccess %v", !rsp.IsSuccess)
	}
	if rsp.OrderId != order.ID {
		t.Errorf("  resp.OrderId != order.ID %v !=%v", resp.OrderId, order.ID)
	}
	if rsp.VendorId != vendor.ID {
		t.Errorf("  rsp.VendorId != vendor.ID %v !=%v", rsp.VendorId, vendor.ID)
	}
	if rsp.TripId != trip.ID {
		t.Errorf("  rsp.TripId != trip.ID %v !=%v", rsp.TripId, trip.ID)
	}
	if rsp.UpdatedTripStatus != string(model.ASSIGNED) {
		t.Errorf(" rsp.UpdatedTripStatus !=string(model.ASSIGNED) %v !=%v", rsp.UpdatedTripStatus, string(model.ASSIGNED))
	}
}
func TestDelayReportVendorDelay(t *testing.T) {
	///Create Model and insert to Database
	var delayByte []byte
	vendor := model.Vendor{
		Name: "TestVenoder",
	}

	dataBase.Create(&vendor)
	defer dataBase.Where(&vendor).Delete(&vendor)

	order := model.Order{
		VendorId:     vendor.ID,
		DeliveryTime: time.Nanosecond,
	}

	dataBase.Create(&order)
	defer dataBase.Where(&order).Delete(&order)

	trip := model.Trip{
		OrderID: order.ID,
		Status:  model.DELIVERED,
	}
	dataBase.Create(&trip)
	defer dataBase.Where(&trip).Delete(&trip)

	///Create Test

	time.Sleep(time.Microsecond)
	resp, err := SendOrderDelayRequest(restyClient, int(order.ID))

	if err != nil {
		t.Errorf(err.Error(), " response most be notSucces ")
	}
	delayByte, err = json.Marshal(model.CustomDuration(0))
	if err != nil {
		t.Errorf(err.Error(), " response most be notSucces ")
	}
	if !resp.IsSuccess {
		t.Errorf("!resp.IsSuccess  = %v", !resp.IsSuccess)
	}
	if resp.OrderId != order.ID {
		t.Errorf("  resp.OrderId != order.ID %v !=%v", resp.OrderId, order.ID)
	}
	if resp.TripStatus != "" {
		t.Errorf(" resp.TripStatus !=null %v ", resp.TripStatus)
	}
	if string(delayByte[1:len(delayByte)-1]) != resp.OverallDelayTime {
		t.Errorf("  string(delayByte[1:len(delayByte)-1]) != resp.OverallDelayTime  %v != %v ", string(delayByte[1:len(delayByte)-1]), resp.OverallDelayTime)
	}

	rsp, err := SenVendorDelayRequest(restyClient, int(vendor.ID))
	if err != nil {
		t.Errorf(err.Error(), " response most be notSucces ")
	}
	if !rsp.IsSuccess {
		t.Errorf("!resp.IsSuccess  = %v", !rsp.IsSuccess)
	}
	if rsp.VendorId != vendor.ID {
		t.Errorf("  rsp.VendorId != vendor.ID %v !=%v", rsp.VendorId, vendor.ID)
	}

	if len(rsp.Delays) != 1 {
		t.Errorf("   len(rsp.Delays)!=1  %v", len(rsp.Delays))
	}

}
