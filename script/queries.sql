-- get Orders whit bad trips
SELECT *
FROM orders
         LEFT JOIN trips ON orders.id = trips.order_id
WHERE (trips.status IS NULL OR trips.status NOT IN ('AtVendor', 'PickedDelivered', 'ASSIGNED'));

-- -- get Orders whit delay
SELECT orders.*,
       t.*,
       orders.delivery_time,
       SUM(delays.time) AS sumOfDelay
FROM orders
         LEFT JOIN delays ON orders.id = delays.order_id
         LEFT JOIN trips t on orders.id = t.order_id
GROUP BY orders.id, orders.created_at, orders.delivery_time, t.id
HAVING (orders.created_at +
        ((orders.delivery_time + COALESCE(SUM(delays.time), 0)) / 1000) * INTERVAL '1 microsecond') < NOW();


-- -- get Orders whit delay and having bad status
SELECT orders.*,
       t.*,
       orders.delivery_time,
       SUM(delays.time) AS sumOfDelay
FROM orders
         LEFT JOIN delays ON orders.id = delays.order_id
         LEFT JOIN trips t on orders.id = t.order_id
GROUP BY orders.id, orders.created_at, orders.delivery_time, t.id, t.status
HAVING (orders.created_at +
        ((orders.delivery_time + COALESCE(SUM(delays.time), 0)) / 1000) * INTERVAL '1 microsecond') < NOW()
   and (t.status IS NULL OR t.status NOT IN ('AtVendor', 'PickedDelivered', 'ASSIGNED'));;


-- -- get Orders whit delay and having good status
SELECT orders.*,
       t.*,
       orders.delivery_time,
       SUM(delays.time) AS sumOfDelay
FROM orders
         LEFT JOIN delays ON orders.id = delays.order_id
         LEFT JOIN trips t on orders.id = t.order_id
GROUP BY orders.id, orders.created_at, orders.delivery_time, t.id, t.status
HAVING (orders.created_at +
        ((orders.delivery_time + COALESCE(SUM(delays.time), 0)) / 1000) * INTERVAL '1 microsecond') < NOW()
   and (t.status IN ('AtVendor', 'PickedDelivered', 'ASSIGNED'));;


-- -- get Orders whitOut delay
SELECT orders.*,
       t.*,
       orders.delivery_time,
       SUM(delays.time) AS sumOfDelay
FROM orders
         LEFT JOIN delays ON orders.id = delays.order_id
         LEFT JOIN trips t on orders.id = t.order_id
GROUP BY orders.id, orders.created_at, orders.delivery_time, t.id
HAVING (orders.created_at +
        ((orders.delivery_time + COALESCE(SUM(delays.time), 0)) / 1000) * INTERVAL '1 microsecond') > NOW();


-- -- get Orders whitOut delay and having bad status
SELECT orders.*,
       t.*,
       orders.delivery_time,
       SUM(delays.time) AS sumOfDelay
FROM orders
         LEFT JOIN delays ON orders.id = delays.order_id
         LEFT JOIN trips t on orders.id = t.order_id
GROUP BY orders.id, orders.created_at, orders.delivery_time, t.id, t.status
HAVING (orders.created_at +
        ((orders.delivery_time + COALESCE(SUM(delays.time), 0)) / 1000) * INTERVAL '1 microsecond') > NOW()
   and (t.status IS NULL OR t.status NOT IN ('AtVendor', 'PickedDelivered', 'ASSIGNED'));;


-- -- get Orders whitOut delay and having good status
SELECT orders.*,
       t.*,
       orders.delivery_time,
       SUM(delays.time) AS sumOfDelay
FROM orders
         LEFT JOIN delays ON orders.id = delays.order_id
         LEFT JOIN trips t on orders.id = t.order_id
GROUP BY orders.id, orders.created_at, orders.delivery_time, t.id, t.status
HAVING (orders.created_at +
        ((orders.delivery_time + COALESCE(SUM(delays.time), 0)) / 1000) * INTERVAL '1 microsecond') > NOW()
   and (t.status IN ('AtVendor', 'PickedDelivered', 'ASSIGNED'));;










